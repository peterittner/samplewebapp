package it.ittner.samplewebapp;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

@SuppressLint("SetJavaScriptEnabled")
public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		WebView myWebView = (WebView) findViewById(R.id.webview);
		myWebView.getSettings().setDatabaseEnabled(true);
		myWebView.setWebViewClient(new WebViewClient());
		WebSettings webSettings = myWebView.getSettings();
		
		webSettings.setDatabasePath("/data/data/it.ittner.samplewebapp/databases/");
		webSettings.setJavaScriptEnabled(true);
		webSettings.setDomStorageEnabled(true);
		myWebView.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return (event.getAction() == MotionEvent.ACTION_MOVE);
			}
		});
		myWebView.setVerticalScrollBarEnabled(false);
		myWebView.setVerticalScrollBarEnabled(false);
		myWebView.loadUrl("file:///android_asset/app/index.html");
	}
}
