# README #

This is a sample application that demonstrates the following things:
* Embedd a responsive html-site into a native android application
* Persistence with LocalStorage within the browser
* AngularJS for View-Model-Binding
* AngularJS to peridically synchronize data through a rest interface
* Twitter Bootstrap to prettify the appearance and make it responsive

### What the App does ###

The App lets you enter some locations. It will persist them into your browsers local storage.
It also lets you delete your entries. It will peridocally update and display the tempratures for your entered locations. It updates the entries asynchronously. That means it does not update them all together but recognizes when an entries grew too old and therefore updates it.

### Interesting Files ###

* index.html (View Layer)
* app.js (Model Layer)
* MainActivity.java (Android WebView Settings)

### Contribution ###

Feel free to contribute any improvments as this is open source software.

### References ###

* [https://angularjs.org/](https://angularjs.org/)
* [http://brian.io/lawnchair/](http://brian.io/lawnchair/)
* [http://getbootstrap.com/](http://getbootstrap.com/)
* [http://developer.android.com/guide/webapps/webview.html](http://developer.android.com/guide/webapps/webview.html)
* [http://developer.android.com/guide/topics/manifest/uses-permission-element.html](http://developer.android.com/guide/topics/manifest/uses-permission-element.html)