

/*
 TODO:
 Speichern von Temperatur
 Nachladen von veralteter Temperatur
 */

// uses local-storage
//console.log('init dom apdater');
//var local_store = new Lawnchair({name:'testing', adapter:'dom'}, function(store) {});
//
//// uses indexed-db
//console.log('init indexed-db apdater');
//var db_store = new Lawnchair({name:'ittner', adapter:'dom'}, function(store) {});
//
//// uses in-memory db
//console.log('init memory apdater');
//var memory_store = new Lawnchair({name:'testing', adapter:'memory'}, function(store) {});
//
// uses file	
//console.log('init file-system apdater');
//var file_store = new Lawnchair({name:'testing', adapter:'html5-filesystem'}, function(store) {});


// select store

Date.prototype.addHours= function(hrs){
    this.setHours(this.getHours()+hrs);
    return this;
}

var app = angular.module('sampleApp', []);

app.controller('weatherController', function ($scope, $http, $timeout, $interval) {

    // Model
    $scope.locations = [];
    $scope.currentPlace = '';


    // Local Storage
    $scope.store = new Lawnchair({name: 'locations', adapter: 'dom'}, function (store) {
    });

    // Handlers

    $scope.loadLocations = function () {
        $scope.store.all(function (alldata) {
            $scope.locations = alldata;

            $scope.locations.forEach(function (loc) {
                $http.get('http://api.openweathermap.org/data/2.5/weather?q=' + loc.place)
                        .success(function (data, status, headers, config) {

                            var delay = Math.floor((Math.random() * 5) + 1); // 1-5
                            var MS = 1000;
                            $timeout(function () {
                                console.log('Got final result after ' + delay + ' second(s).');
                                if (data.cod == 200) {
                                    loc.temp = data.main.temp;
                                    // Update Temp in db
                                    $scope.store.save({key:loc.key, time:new Date(), place:loc.place, temp:loc.temp});
                                }
                            }, delay * MS);
                        });
            });


        });
    }
    
    // Update old temps
    $interval(function() {
        
        $scope.locations.forEach(function(location) {
            var oneHour = 60 * 1000;// * 60;
            var now = new Date();
            console.log(location.place + ' - age: ' + (now - new Date(location.time)) + 'ms');
            if((now - new Date(location.time)) > oneHour) {
                console.log('too old: ' + location.place);
                location.temp = null;
                $http.get('http://api.openweathermap.org/data/2.5/weather?q=' + location.place)
                .success(function (data, status, headers, config) {
                    var delay = Math.floor((Math.random() * 5) + 1); // 1-5
                    var second = 1000;
                    $timeout(function () {
                        console.log('Got final result after ' + delay + ' second(s).');
                        if (data.cod == 200) {
                            location.temp = data.main.temp;
                            location.time = new Date();
                            // Update Temp in db
                            $scope.store.save({key:location.key, time:location.time, place:location.place, temp:location.temp});
                        }
                    }, delay * second);
                });
            }
        });
    }, 1000);

    $scope.saveLocation = function () {

        var obj = {time: new Date(), place: $scope.currentPlace, temp: null};
        console.log('Saving: ' + obj.time + ' - ' + obj.place);
        $scope.store.save(obj);
        $scope.loadLocations();
        $scope.currentPlace = '';
    }

    $scope.deleteLocation = function (index) {
        del = $scope.locations[index];
        $scope.store.remove(del.key);
        $scope.loadLocations();
    }

    $scope.deleteAll = function () {
        $scope.store.nuke();
        $scope.loadLocations();
    }

    // init the scope
    $scope.loadLocations();

});







